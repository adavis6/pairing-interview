# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :bare_lv,
  ecto_repos: [BareLv.Repo]

# Configures the endpoint
config :bare_lv, BareLvWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "LmS9w24kLMjbJf6ARa/zRjIR+RwWKIFEQRIgV3MjJTGbvWdkJ84Tql1DDwvA4pSF",
  render_errors: [view: BareLvWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: BareLv.PubSub,
  live_view: [signing_salt: "pdcr6T1U"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
