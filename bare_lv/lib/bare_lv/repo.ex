defmodule BareLv.Repo do
  use Ecto.Repo,
    otp_app: :bare_lv,
    adapter: Ecto.Adapters.Postgres
end
